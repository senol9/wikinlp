Employing artificial neural networks for constructing metadata-based model to automatically select an appropriate data visualization technique
Code structure visualization using 3D-flythrough
Visualization of combinatorial models and test plans
Software evolution visualization techniques and methods - A systematic review
The effects of visualization and interaction techniques on feature model configuration
Case studies of optimized sequence diagram for program comprehension
Synchronized static and dynamic visualization in a web-based programming environment
GiveMe Trace: A Software Evolution Traceability Support Tool
Titanium: Visualization of TTCN-3 system architecture
NetFork: Mapping time to space in network visualization
Proposing and assessing a software visualization approach based on polymetric views
Knowledge discovery in software teams by means of evolutionary visual software analytics
Understanding JavaScript event-based interactions with clematis
A Method of Handling Metamodel Based on XML Database for SW Visulization
SW Visualization Framework for Safe Software
Algorithm visualizations as a way of increasing the quality in computer science education
Requirements for tools for comprehending highly specialized assembly language code and how to elicit these requirements
Hybrid effort estimation of changes in agile software development
Unsupervised learning for detecting refactoring opportunities in service-oriented applications
An educational game for teaching search algorithms
TDDViz: Using software changes to understand conformance to Test Driven Development
Runtime visualization and verification in JIVE
Spatiotemporal data visualisation for homecare monitoring of elderly people
On the effective visualisation of dynamic attribute cascades
Interactive visualization system for monitoring support targeting multiple BBS threads
Utilizing program visualization in learning hardware programming: Effects of engagement level
DocTool - A tool for visualizing software projects using graph database
Visual analytics of software structure and metrics
Stable Voronoi-based visualizations for software quality monitoring
SMNLV: A small-multiples node-link visualization supporting software comprehension by displaying multiple relationships in software structure
UrbanIt: Visualizing repositories everywhere
Visualising software as a particle system
Hierarchical software landscape visualization for system comprehension: A controlled experiment
SPIDER SENSE: Software-engineering, networked, system evaluation
Experiences from performing software quality evaluations via combining benchmark-based metrics analysis, software visualization, and expert assessment
Explora: A visualisation tool for metric analysis of software corpora
A survey on goal-oriented visualization of clone data
Advancing data race investigation and classification through visualization
A visual support for decomposing complex feature models
Vestige: A visualization framework for engineering geometry-related software
Interactive tag cloud visualization of software version control repositories
Unified model for software engineering data
Live object exploration: Observing and manipulating behavior and state of Java objects
Live visualization of GUI application code coverage with GUITracer
Facilitating Information Management in Integrated Development Environments through Visual Interface Enhancements
A magnet-and-spring based visualization technique for enhancing the manipulation of requirements artifacts
Iterative cohort analysis and exploration
Enhancing software visualization with information retrieval
Thread city: Combined visualization of structure and activity for the exploration of multi-threaded software systems
Visual and Audio Monitoring of Island Based Parallel Evolutionary Algorithms
Code Pulse: Real-time code coverage for penetration testing activities
Pan-Tetris: An interactive visualisation for Pan-genomes
Combining dynamic analysis and visualization to explore the distribution of unit test suites
Comparing Trace Visualizations for Program Comprehension through Controlled Experiments
How to master challenges in experimental evaluation of 2D versus 3D software visualizations
Beyond the classical monoscopic 3D in graph analytics: An experimental study of the impact of stereoscopy
Understanding business ecosystem dynamics: A data-driven approach
Readability metric feedback for aiding node-link visualization designers
Introducing aesthetics to software visualization
Visualizing the evolution of subsystems
An interactive approach for inspecting software system measurements
VisMinerTD: An open source tool to support the monitoring of the technical debt evolution using software visualization
Data and control flow visualization by transforming software into schematic diagrams
Assessing the use of slicing-based visualizing techniques on the understanding of large metamodels
Code smells detection and visualization of software systems
Teaching information aesthetics as a research class in China
Evaluating the capabilities of enterprise architecture modeling tools for visual analysis
Visually characterizing source code changes
A flowchart-based multi-agent system for assisting novice programmers with problem solving activities
Resolving cyclic dependencies between packages with enriched dependency structural matrix
Validation of software visualization tools: A systematic mapping study
Supporting regression test scoping with visual analytics
Information visualization for agile software development
On visual assessment of software quality
Towards self-adaptive IDEs
The integrated software visualization model to support novices’ program comprehension
Illustrating software modifiability - Capturing cohesion and coupling in a force-optimized graph
The challenge of helping the programmer during debugging
Visual storytelling of development sessions
A visualization tool recording historical data of program comprehension tasks
Sourceminer: Towards an extensible multi-perspective software visualization environment
The Solid* toolset for software visual analytics of program structure and metrics comprehension: From research prototype to product
RegViz: Visual debugging of regular expressions
Multivariate networks in software engineering
GCLViz: Garbage collection vs. latency visualization
Slicing-based techniques for visualizing large metamodels
Visualization system for monitoring bug update information
Viewing object-oriented software with metricattitude: An empirical evaluation
Source code revision history visualization tools: Do they work and what would it take to put them to work?
Software feathers: Figurative visualization of software metrics
Streamlining code smells: Using collective intelligence and visualization
A framework to measure and visualize class coupling
eCITY+: A tool to analyze software architectural relations through interactive visual support
Taupe: Visualizing and analyzing eye-tracking data
Avispa: a tool for analyzing software process models
Visualising Java coupling and fault proneness
Tackling the requirements jigsaw puzzle
AniMatrix: A matrix-based visualization of software evolution
On the benefits of component-defined real-time visualization of robotics software
Gamification of System-on-Chip design
Visualizing protected variations in evolving software designs
How developers visualize compiler messages: A foundational approach to notification construction
POETIC: Interactive solutions to alleviate the reversal error in student-professor type problems
Feature relations graphs: A visualisation paradigm for feature constraints in software product lines
Gridvis: Visualisation of island-based parallel genetic algorithms
GridVis: Visualisation of island-based parallel genetic algorithms
ChronoTwigger: A visual analytics tool for understanding source and test co-evolution
Debugging and visualization techniques for multithreaded programs: A survey
Pareto frontier visualization in the development of release rules for hydro-electrical power stations
A systematic review of software architecture visualization techniques
Visual requirements analytics: A framework and case study
Using visual information analysis to explore complex patterns in the activity of designers
Evaluating the usability of a visual feature modeling notation
Visualizing software structure understandability
Reverse engineering and visualization of the reactive behavior of PLC applications
Magnify - A new tool for software visualization
Using HTML5 visualizations in software fault localization
CodeMetrpolis - A minecraft based collaboration tool for developers
DEVis: A tool for visualizing software document evolution
IMMV: An interactive multi-matrix visualization for program comprehension
Sextant: A tool to specify and visualize software metrics for Java source-code
Visualizing software dynamicities with heat maps
Towards interactive visualization support for pairwise testing software product lines
Finding structures in multi-type code couplings with node-link and matrix visualizations
SourceMiner: A multi-perspective software visualization environment
Testware visualized: Visual support for testware reorganization
Cooperation wordle using pre-attentive processing techniques
Software evolution visualization: A systematic mapping study
A review of generic program visualization systems for introductory programming education
SArF map: Visualizing software architecture from feature and layer viewpoints
Visualization of unit and selective regression software tests
GosperMap: Using a gosper curve for laying out hierarchical data
Interactive Visual Analytics for Efficient Maintenance of Model Transformations
Automated metric visualizations for analyzing source code repositories
BugMap: A topographic map of bugs
Interactive software maps for web-based source code analysis
Do background colors improve program comprehension in the #ifdef hell?
Development of visualization method for WAMS signal situation
Complexity plots
Interactive ambient visualizations for soft advice
Understanding software evolution with software cities
Visualizing offscreen elements of node-link diagrams
A fine-grained, customizable debugger for aspect-oriented programming
Test City Metaphor for Low Level Tests Restructuration in Test Database
A visualization concept for high-level comparison of process model versions
Program visualization: Effect of viewing vs. responding on student learning
Codemetropolis ' code visualisation in minecraft
Towards the analysis of software projects dependencies: An exploratory visual study of software ecosystems
A study on enhancing timeline-like visualization with verbal text
Visualising process model hierarchies
Multiple coordinated views to support aspect mining using program slicing
A Generic macroscopic topology of software networks - A quantitative evaluation
Software visualisation through video games
The state-of-the art tools to visualize software [O estado-da-arte das ferramentas de visualiza̧c̃ao de software]
Demonstrating the stalling events with instantaneous total power consumption in smartphone-based live video streaming
Mobile applications software testing methodology
Rank-directed layout of UML class diagrams
Analyzing long-running controller applications for specification violations based on deterministic replay
A visual analysis approach to support perfective software maintenance
Improving visualization of large hierarchical clustering
Seeing errors: Model driven simulation trace visualization
Design considerations for optimizing storyline visualizations
Analysis on the vehicle steering stability visualization simulation technology
ViSA: Visualization of sorting algorithms
Extending recommendation systems with software maps
Refining code ownership with synchronous changes
Effective visualization of conceptual class diagrams
MetricAttitude: A visualization tool for the reverse engineering of object oriented software
Evaluating component architecture visualization tools: Criteria and case study
Understanding API usage to support informed decision making in software maintenance
Spy: A flexible code profiling framework
A GIS-based management and publication framework for data handling of numerical model results
Ontology-aided annotation, visualization, and generalization of geological time-scale information from online geological map services
Debug concurrent programs with visualization and inference of event structure
On the use of software visualization to analyze software evolution: An interactive differential approach
Evidence-based timelines for agile project retrospectives - A method proposal
Improvement of a visualization technique for the passage rate of unit testing and static checking and its evaluation
mJeliot: ICT support for interactive teaching of programming
A framework for visualizing model-driven software evolution - Its evaluation
Mjeliot: A tool for enhanced interactivity in programming instruction
Visualizing the refactoring of classes via clustering
Comparing the collaborative and independent viewing of program visualizations
MosaiCode: Visualizing large scale software - A tool demonstration

Constellation visualization: Augmenting program dependence with dynamic information
3D hierarchical edge bundles to visualize relations in a software city metaphor
What you see is what you asked for: An effort-based transformation of code analysis tasks into interactive visualization scenarios
Visual support for porting large code bases
E-quality: A graph based object oriented software quality visualization tool
Combining dynamic program viewing and testing in early computing courses
A visualization and modeling tool for security metrics and measurements management
Architectural design decision visualization for architecture design: Preliminary results of a controlled experiment
On the role of design in information visualization
Effectiveness of program visualization in learning Java: A case study with Jeliot 3
Visualization techniques for application in interactive product configuration
Collective code bookmarks for program comprehension
Java replay for dependence-based debugging
A visualization technique for the passage rates of unit testing and static checking with caller-callee relationships
An ontology-enabled user interface for simulation model construction and visualization
Applying source code analysis techniques: A case study for a large mission-critical software system
Monitoring code quality and development activity by software maps
Using blender to design and implement data visualization components for X3D and X3DOM
Software systems as cities: A controlled experiment
The code orb - Supporting contextualized coding via at-a-glance views (NIER track)
Visualization of the static aspects of software: A survey
A study on the application of the PREViA approach in modeling education
A controlled experiment for program comprehension through trace visualization
Visualizing web search results using glyphs: Design and evaluation of a flower metaphor
Coherent software cities: Supporting comprehension of evolving software systems
Recovery and analysis of transaction scope from scattered information in Java Enterprise Applications
Aspect mining using self-organizing maps with method level dynamic software metrics as input vectors
Evaluating adaptation behavior of adaptive systems
Architectural decision modeling with reuse: Challenges and opportunities
Dependence cluster visualization
Embedding spatial software visualization in the IDE: An exploratory study
Metric pictures: The approach and applications
Comprehending Ajax Web applications by the DynaRIA tool
Using the magnet metaphor for multivariate visualization in software management
An interactive ambient visualization for code smells
Representing development history in software cities
Off-screen visualization techniques for class diagrams
Visualization of large software projects by using advanced techniques
Immediacy through interactivity: Online analysis of run-time behavior
Fault forest visualization
Package Fingerprints: A visual summary of package interface usage
Jype - A program visualization and programming exercise tool for python
Identifying code smells with multiple concern views
Towards anomaly comprehension: Using structural compression to navigate profiling call-trees
Automated construction of memory diagrams for program comprehension
Visual readability analysis: How to make your writings easier to read
Visualization of C++ template metaprograms
IChase: Supporting exploration and awareness of editing activities on Wikipedia
ITVT: An image testing and visualization tool for image processing tasks
An empirical study on the efficiency of different design pattern representations in UML class diagrams
An interactive tool for data structure visualization and algorithm animation: Experiences and results
Patrools: Visualizing the polymorphic usage of class hierarchies
Automatic animation for time-varying data visualization
Visual search in dynamic 3D visualisations of unstructured picture collections
Design of a visualization system of sequential logic chip based on SVG
Designing computer games to teach algorithms
Integrating categories of algorithm learning objective into algorithm visualization design: A proposal
Algorithm Visualization: The state of the field
Visualizing and assessing a compositional approach of business process design
In situ software visualisation
A demo on using visualization to aid run-time verification of dynamic service systems
GMap: Visualizing graphs and clusters as maps
Caleydo: Design and evaluation of a visual analysis framework for gene expression data in its biological context
Software Cartography: Thematic software visualization with consistent layout
Rigi-An environment for software reverse engineering, exploration, visualization, and redocumentation
Visual and algorithmic tooling for system trace analysis: A case study
Applying the metro map to software development management
Interaction promotes collaboration and learning: Video analysis of algorithm visualization use during collaborative learning
Analysis and visualization of information quality of technical documentation
Gaining insight into programs that analyze programs - By visualizing the analyzed program
Visualizing multiple program executions to assist behavior verification
Sv3D meets eclipse
Simplifying algorithm learning using serious games
Enabling the evolution of J2EE applications through reverse engineering and quality assurance
Pedagogically effective effortless algorithm visualization with a PCIL
A novel sorting animation: Permuting picture pixels
The effect of visualizing roles of variables on student performance in an introductory programming course
Projecting code changes onto execution traces to support localization of recently introduced bugs
Extraction and visualization of call dependencies for large C/C++ code bases: A comparative study
Ontology-driven visualization of architectural design decisions
Scouting requirements quality using visual representations
EvoSpaces - Multi-dimensional navigation spaces for software evolution
Visualization of software and systems as support mechanism for integrated software project control
Improved feedback for architectural performance prediction using software cartography visualizations
Sonification design guidelines to enhance program comprehension
Trace visualization for program comprehension: A controlled experiment
Visual exploration of large-scale evolving software
Optimized data transfer for time-dependent, GPU-based glyphs
Visualizing metrics on areas of interest in software architecture diagrams
How does algorithm visualization affect collaboration?: Video analysis of engagement and discussions
SQuAVisiT: A flexible tool for visual software analytics
Visualizing the runtime behavior of embedded network systems: A toolkit for TinyOS
An interactive simulation and analysis software for solving TSP using Ant Colony Optimization algorithms
Towards realism in drawing areas of interest on architecture diagrams
A Framework for Reverse Engineering Large C++ Code Bases
An overview of 3D software visualization
Software visualization using a Treemap-hypercube metaphor
Remixing visualization to support collaboration in software maintenance
A testbed for visualizing sensornet behavior
Consistent layout for thematic software maps
Visual exploration of large-scale system evolution
A hybrid query engine for the structural analysis of Java and AspectJ programs
CCVisu: Automatic visual software decomposition
Supporting system development by novice software engineers using a tutor-based software visualization (TubVis) approach
Representing unit test data for large scale software development
Towards a better control of change impact propagation
Software visualization for end-user programmers: Trial period obstacles
Visualization of software evolution
A catalogue of lightweight visualizations to support code smell inspection
Visually localizing design problems with disharmony maps
Exploring the composition of unit test suites
Analyzing the reliability of communication between software entities using a 3D visualization of clustered graphs
Ellimaps. A technique based on the law of inclusion to represent hierarchies with weighted nodes [Ellimaps, une technique basée sur la loi d'inclusion pour représenter des hiérarchies avec nœuds pondéré s]
Exploring the evolution of software quality with animated visualization
A usability study on the use of multi-context visualization
Visualizing concurrency control algorithms for real-time database systems
A toolkit for visualizing the runtime behavior of TinyOS applications
A visualization system of source codes using FOL
Visually summarising software change
Package reference fingerprint: A rich and compact visualization to understand package relationships
Analyzing the performance of a cluster-based architecture for immersive visualization systems
Model inspection in dicodess
Communicating software architecture using a unified single-view visualization
Tool users requirements classification: How software visualization tools measure up
On requirements visualization
Interface and visualization metaphors
A small observatory for super-repositories
On the effectiveness of visualizations in a theory of computing course
FASTDash: A visual dashboard for fostering awareness in software teams
Scenario explorer: Interactive visualization of use cases
Tracking objects to detect feature dependencies
JThreadSpy: Teaching multithreading programming by analyzing execution traces
An Evaluation of the Effortless Approach to Build Algorithm Animations with WinHIPE
Visual data mining and analysis of software repositories
Creating and visualizing test data from programming exercises
Visual assessment of software evolution
VizEval: An experimental system for the study of program visualization quality
Using social agents to visualize software scenarios
Lightweight visualizations for inspecting code smells
The importance of interactive questioning techniques in the comprehension of algorithm animations
Visualisations of Execution Traces (VET): An Interactive Plugin-Based Visualisation Tool
Analyzing feature implementation by visual exploration of architecturally-embedded call-graphs
TAUPE: Towards understanding program comprehension
Co-change visualization applied to PostgreSQL and ArgoUML: (MSR challenge report)
Debugging with software visualization and contract discovery
Software evolution: Analysis and visualization
Code Thumbnails: Using spatial memory to navigate source code
Visualization of areas of interest in software architecture diagrams
Transparency, holophrasting, and automatic layout applied to control structures for visual dataflow programming languages
A system for visualizing binary component-based program structure with component functional size
Software visualizations for improving and measuring the comprehensibility of source code
An evaluation of a symbol table visualization tool
Theories, tools and research methods in program comprehension: Past, present and future
A tool for interactive learning of data structures and algorithms
Informing the design of pipeline-based software visualisations
Visualization-based analysis of quality for large-scale software systems
3D visualization techniques to support slicing-based program comprehension
Source model analysis using the JJTraveler visitor combinator framework
Visualizing feature evolution of large-scale software based on problem and modification report data
Interactive, immersive visualization for indoor environments: Use of augmented reality, human-computer interaction and building simulation
Fault localization using visualization of test information

Approaches to supporting software visual notation exchange


Plugging-in Visualization: Experiences Integrating a Visualization Tool with Eclipse
Mapping the royal road and other hierarchical functions
SoftArch: Tool support for integrated software architecture development
Re-documenting, visualizing and understanding software system using DocLike viewer
Visualizing interactions in distributed Java applications
Visualisation methods for supporting the exploration of high dimensional problem spaces in engineering design
Exploring the role of visualization and engagement in computer science education
The effectiveness of control structure diagrams in source code comprehension activities
The role of concepts in program comprehension
Architecture-based visualisation of computer based systems
Metrics-based 3D visualization of large object-oriented programs
Dynamic component program visualization
Visual attention and representation switching during Java program debugging: A study using the restricted focus viewer

Role of comprehension in software inspection
Visualization of reusable software assets
Understanding some software quality aspects from architecture and design models
ReVis: Reverse engineering by clustering and visual object classification
