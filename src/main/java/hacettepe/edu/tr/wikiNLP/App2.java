package hacettepe.edu.tr.wikiNLP;

import java.util.Map;
import java.util.Map.Entry;

import hacettepe.edu.tr.wikiNLP.ibm.IBMModel2;
import hacettepe.edu.tr.wikiNLP.ibm.IBMModelApp;
import hacettepe.edu.tr.wikiNLP.ibm.WordAligner;

public class App2 {
	
	public static void main(String[] args) {
    	long startTime = System.currentTimeMillis();
    	WordAligner model = new IBMModel2();
    	IBMModelApp modelApp = new IBMModelApp(model,Constant.EN_IT_WIKIPEDIA_ENTRIES_XML);
    	evaluate(model);
        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println((totalTime/1000) + "sn");
    	Map<String, String> evalMap = FileController.readToMapFile("resources//ITEN_Eval.dic");
    	int i=0;
    	for(Entry<String, String> e:evalMap.entrySet()){
    		String maxSimilar = model.getMaxSimilar(e.getValue());
    		if(maxSimilar!=null&&maxSimilar.equals(e.getKey())){
    			++i;
    		}
    		System.out.println("Result : " + i);
    	}
	}
	
	private static void evaluate(WordAligner model){
		
	}

}
