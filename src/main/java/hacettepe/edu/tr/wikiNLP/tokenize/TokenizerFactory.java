package hacettepe.edu.tr.wikiNLP.tokenize;

import java.io.Reader;
import java.util.ArrayList;
import java.util.List;


/**
 * For flexibility lets move the configuration stuff for the tokenizer to this class. 
 * 
 * 
 * @author gonenc
 *
 */
public class TokenizerFactory {
	
	public WordTokenizer createWordTokenizer(Document document) {
		WordTokenizer tokenizer = new WordTokenizer(document.openReader());
		return tokenizer;
	}
	
	public List<String> getWordList(String sentence){
		List<String> resultList = new ArrayList<>();
		WordTokenizer w =createWordTokenizer(new StringDocument(sentence));
		String token=null;
		while((token=w.nextToken())!=null){
			resultList.add(token);
		}
		return resultList;
	}
	
	public static void main(String[] args) {
		TokenizerFactory t = new TokenizerFactory();
		WordTokenizer w =t.createWordTokenizer(new StringDocument("The Pack (1977 film)"));
		String token=null;
		while((token=w.nextToken())!=null){
			System.out.println(token.intern());
		}
	}
	
//	<article lang="en" name="The Pack (1977 film)"> >>>> <article lang="it" name="Il branco (film 1977)">
//	<article lang="en" name="Hornets’ Nest"> >>>> <article lang="it" name="I lupi attaccano in branco">
//	<article lang="en" name="Il branco"> >>>> <article lang="it" name="Il branco (film 1994)">
//	<article lang="en" name="Herd immunity"> >>>> <article lang="it" name="Immunità di branco">

}
