package hacettepe.edu.tr.wikiNLP;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import hacettepe.edu.tr.wikiNLP.ibm.IBMModel1;
import hacettepe.edu.tr.wikiNLP.ibm.IBMModelApp;
import hacettepe.edu.tr.wikiNLP.ibm.SentencePair;
import hacettepe.edu.tr.wikiNLP.ibm.Word;

public class App1 
{
	public static final double THRESHOLD_VALUE = 0;
	
    public static void main( String[] args )
    {
    	long startTime = System.currentTimeMillis();
    	IBMModel1 model = new IBMModel1();
    	IBMModelApp modelApp = new IBMModelApp(model,Constant.EN_ES_WIKIPEDIA_ENTRIES_XML);
    	evaluate(model);
    	//all words more than threshold.
//    	Map<String, String> allSimilarWordMap = getAllMaxTranslationsWithMap(modelApp, model);
        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        System.out.println((totalTime/1000) + "sn");
    }
    
    private static void evaluate(IBMModel1 model){
    	Map<String, String> rightMap = new HashMap<>();
    	List<Word> wordList = new ArrayList<>();
    	List<Word> sWordList = new ArrayList<>();
    	List<String> anList = new ArrayList<>();
    	Map<String, String> evalMap = FileController.readToMapFile("resources//ESEN_Eval.dic");
    	int i=0, y=0, x=0, k=0, t=0, c=0, m=0;
    	for(Entry<String, String> e:evalMap.entrySet()){
    		String maxSimilar = model.getMaxSimilar(e.getValue());
    		String commonSimilar = model.getCommonSimilar(e.getValue());
    		String maxSimilarAsTarget = model.getMaxSimilarAsTarget(e.getKey());
    		String maxOptimumAsTarget = model.getOptimumSimilarAsTarget(e.getKey());
//    		String maxSimilar = model.getMaxSimilarWithTreshold(e.getValue());
//    		if(maxSimilar==null){
//    			maxSimilar=e.getKey();
//    			++y;
//    		}
    		if(commonSimilar!=null &&commonSimilar.equals(e.getKey())){
    			++c;
    		}
    		if(maxSimilarAsTarget!=null&&maxSimilarAsTarget.equals(e.getValue())){
    			++t;
    		}
    		if(maxSimilar!=null&&maxSimilar.equals(e.getKey())){
    			++i;
    		}
    		wordList.add(new Word(e.getKey(),e.getValue(),maxSimilar,maxSimilarAsTarget,maxOptimumAsTarget,model.getMaxIndex(e.getValue()),model.getMaxIndexAsTarget(e.getKey())));
    		if(e.getValue().equals(e.getKey())){
    			++m;
    		}else if(maxOptimumAsTarget!=null&&maxOptimumAsTarget.equals(e.getValue())){
//    			rightMap.put(e.getKey(), e.getValue());
    			++m;
    		}else{
    			anList.add(e.getKey() + " (" + e.getValue()+ ")" + " >>>> " + model.getSimilarValuesAsTarget(e.getKey()));
    	 		sWordList.add(new Word(e.getKey(),e.getValue(),maxSimilar,maxSimilarAsTarget,maxOptimumAsTarget,model.getMaxIndex(e.getValue()),model.getMaxIndexAsTarget(e.getKey())));
    	    	
    		}
    	}
    	System.out.println("Total common match : " + c);
    	System.out.println("Total union match : " + m);
    	System.out.println("Total match : " + i);
    	System.out.println("Total target match : " + t);
//    	System.out.println("Data inadequate : " + y);
//    	System.out.println("Same input and output : " + x);
//    	System.out.println("Not first but in list : " + k);
//    	FileController.writeMapToFile(rightMap, "resources//en_it.txt");
    	//Collections.sort(wordList);
    	//FileController.writeListToFile(wordList, "resources//en_it_d.txt");
    	//Collections.sort(sWordList);
    	//FileController.writeListToFile(sWordList, "resources//en_it_w.txt");
    	//FileController.writeListToFile(anList, "resources//en_it_anlysis.txt");
    }
    
    private static Map<String, String> getAllMaxTranslationsWithMap(IBMModelApp modelApp,IBMModel1 model){
	    Map<String, String> sourceTargetMap = new HashMap<>();
	    for(SentencePair p:modelApp.getTrainingSentencePairs()){
	    	for(String s:p.getSourceWords()){
	    		String d=model.getMaxSimilarWithTreshold(s);
	    		if(d!=null&&!sourceTargetMap.containsKey(s)){
	    			sourceTargetMap.put(s, d);
	    		}
	    	}
	    }
	    return sourceTargetMap;
    }
    
}
