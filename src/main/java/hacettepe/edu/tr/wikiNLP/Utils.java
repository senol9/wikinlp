package hacettepe.edu.tr.wikiNLP;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Utils {
	
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> unsortMap,final boolean isAsc) {

	    List<Map.Entry<K, V>> list =
	            new LinkedList<Map.Entry<K, V>>(unsortMap.entrySet());

	    Collections.sort(list, new Comparator<Map.Entry<K, V>>() {
	        public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
	            return isAsc?(o1.getValue()).compareTo(o2.getValue()):(o2.getValue()).compareTo(o1.getValue());
	        }
	    });

	    Map<K, V> result = new LinkedHashMap<K, V>();
	    for (Map.Entry<K, V> entry : list) {
	        result.put(entry.getKey(), entry.getValue());
	    }

	    return result;

	}
	
	 public static <K, V> Map.Entry<K, V> getEntry(Map<K,V> map,int i)
	    {
	        // check if negetive index provided
	        Set<Map.Entry<K,V>>entries = map.entrySet();
	        int j = 0;

	        for(Map.Entry<K, V>entry : entries)
	            if(j++ == i)return entry;

	        return null;

	    }
	
	 public static <K, V> void printMap(Map<K, V> map) {
	        for (Map.Entry<K, V> entry : map.entrySet()) {
	            System.out.println("Key : " + entry.getKey()
	                    + " Value : " + entry.getValue());
	        }
	    }


}
