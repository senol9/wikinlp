package hacettepe.edu.tr.wikiNLP.ibm;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hacettepe.edu.tr.wikiNLP.Constant;
import hacettepe.edu.tr.wikiNLP.FileController;
import hacettepe.edu.tr.wikiNLP.tokenize.StringDocument;
import hacettepe.edu.tr.wikiNLP.tokenize.TokenizerFactory;
import hacettepe.edu.tr.wikiNLP.tokenize.WordTokenizer;


public class IBMModelApp {
	
	  // SGML patterns
	  public static final Pattern SGML_OPEN_TAG = Pattern.compile("<articlePair id=\"(\\d+)\">");
	  public static final Pattern ARTICLE_NAME_EN = Pattern.compile("<article lang=\"en\" name=\"(.*)\">");
	  public static final Pattern ARTICLE_NAME_IT = Pattern.compile("<article lang=\"es\" name=\"(.*)\">");
	  public static final Pattern CATEGORY_NAME = Pattern.compile("<categories name=\"(.*)\"/>");
	  public static final Pattern SGML_CLOSE_TAG = Pattern.compile("</articlePair>");
	  public static final double TRESSHOLD_OF_SIMILAR = 0.51;
	  private List<SentencePair> trainingSentencePairs;
	  TokenizerFactory tokenizer = new TokenizerFactory();

	  
	  public IBMModelApp(WordAligner model,String trainDataPath){
		  trainingSentencePairs = loadTrainingData(trainDataPath, Integer.MAX_VALUE);
		  model.train(trainingSentencePairs);	
	  }
	
	  private List<SentencePair> loadTrainingData(String path, int maxSentencePairs) {
		    List<SentencePair> sentencePairs = new ArrayList<SentencePair>();
		      List<SentencePair> fileSentences = readAlignedSentences(path);
		      if (sentencePairs.size() + fileSentences.size() <= maxSentencePairs) {
		        sentencePairs.addAll(fileSentences);
		      }
		    return sentencePairs;
		  }
	  
	  private List<SentencePair> readAlignedSentences(String path) {
		    List<SentencePair> sentencePairs = new ArrayList<SentencePair>();
		    try {
		      BufferedReader buffered = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF-8"));
		      String line = buffered.readLine();
		      String engTitle = null;
		      String[] engCategory = null;
		      String itTitle=null;
		      String[] itCategory = null;
		      List<String> engSetList;
		      List<String> itSetList;
		      String engContext = "";
		      String itContext = "";
		      boolean isEngContext=false;
		      boolean isItContext=false;
		      int limit = 0;
		      while (line!=null) {
				if(line.contains("<article lang=\"en\"")){
					engTitle = line;
				}else if(line.contains("<article lang=\"es\"")){
					itTitle = line;
				}
//				if(itTitle!=null && line.contains("<categories name=\"")){
//					itCategory = splitCategory(line);
//				}else if(engTitle!=null && line.contains("<categories name=\"")){
//					if(engCategory==null)
//					engCategory = splitCategory(line);
//					else engCategory=null;
//				}
				
//				if(itTitle!=null && line.contains("</content>")){
//					isItContext=false;
//				}else if(engTitle!=null && line.contains("</content>")){
//					isEngContext=false;
//				}
//				
//				if(isItContext){
//					itContext+=line;
//				}else if(isEngContext){
//					engContext+=line;
//				}
//				
//				if(itTitle!=null && line.contains("<content>")){
//					isItContext=true;
//				}else if(engTitle!=null && line.contains("<content>")){
//					isEngContext=true;
//				}
				
				if(engTitle!=null && itTitle!=null){
					 engSetList = tokenizeAndIntern(engTitle,true);
					 itSetList = tokenizeAndIntern(itTitle,false);
					 if(
//							 !isSimilar(itSetList, engSetList) && 
							 !engSetList.isEmpty() && !itSetList.isEmpty())
					 sentencePairs.add(new SentencePair(engSetList,itSetList));
					 engTitle=null;
					 itTitle=null;
				}
				
//				if(!isItContext && !itContext.equals("") && !engContext.equals("")){
//					++limit;
//					if(limit<1000)
//					sentencePairs.add(new SentencePair(tokenizeAndIntern(itContext),tokenizeAndIntern(engContext)));
//					itContext="";
//					engContext="";
//				}
				
//				if(engCategory!=null && itCategory!=null){
//					if(engCategory.length==itCategory.length){
//						for(int i=0; i<engCategory.length; i++){
//							sentencePairs.add(new SentencePair(tokenizeAndIntern(itCategory[i]), tokenizeAndIntern(engCategory[i])));
//						}
//					}
//					engCategory=null;
//					itCategory =null;
//				}
				line = buffered.readLine();
		      }
		      buffered.close();

		    } catch (IOException e) {
		      throw new RuntimeException(e);
		    }
		    return sentencePairs;
		  }
	  
	  private boolean isSimilar(Set<String> sourceList, Set<String> targetList){
		  double i=0; 
		  for(String s:sourceList){
			  if(targetList.contains(s)){
				  ++i;
			  }
		  }
		  if((i>1)){
			  return true;
		  }
		  return false;
	  }
	  
	  private String[] splitCategory(String line){
		    Matcher mOpen = CATEGORY_NAME.matcher(line);
		    if (mOpen.find()) {
		      return mOpen.group(1).replaceAll("&apos;", " ").replaceAll("&quot;", " ").split("\\|");
		    }
		    return null;
	  }
	  
	  private List<String> tokenizeAndIntern(String line,boolean isSource) {
		    // Strip off SGML and extract segment id
		    Matcher mOpen = ARTICLE_NAME_EN.matcher(line);
		    if (mOpen.find()) {
		      line = mOpen.group(1).replaceAll("&apos;", " ").replaceAll("&quot;", " ");
		    }
		    mOpen = ARTICLE_NAME_IT.matcher(line);
		    if (mOpen.find()) {
			      line = mOpen.group(1).replaceAll("&apos;", " ").replaceAll("&quot;", " ");
			}

		    // Intern the tokens for memory efficiency
		    List<String> tokenList = new ArrayList<>();
			WordTokenizer w =tokenizer.createWordTokenizer(new StringDocument(line.trim()));
			String token=null;
			while((token=w.nextToken())!=null){
				if(token.intern().length()>2)
					tokenList.add(token.intern());
				
			}
			tokenList.remove(null);
		   
		    return tokenList;
		  }
	  
	  
	  public List<SentencePair> getTrainingSentencePairs() {
		return trainingSentencePairs;
	}

	public void setTrainingSentencePairs(List<SentencePair> trainingSentencePairs) {
		this.trainingSentencePairs = trainingSentencePairs;
	}

	public static void main(String[] args) {
		String line = "<categories name=\"1944 romanları|1956 tiyatro oyunları|Agatha Christie romanları|Agatha Christie oyunları|Kapak resmi aranan kitaplar\"/>";
		Matcher mOpen = CATEGORY_NAME.matcher(line);
	    if (mOpen.find()) {
		      System.out.println(mOpen.group(1).split("\\|").length);
		    }
	}

}
