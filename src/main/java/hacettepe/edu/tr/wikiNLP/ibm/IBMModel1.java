package hacettepe.edu.tr.wikiNLP.ibm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import hacettepe.edu.tr.wikiNLP.App1;
import hacettepe.edu.tr.wikiNLP.Constant;
import hacettepe.edu.tr.wikiNLP.FileController;
import hacettepe.edu.tr.wikiNLP.Utils;

public class IBMModel1  implements WordAligner{

	  
	  // TODO: Use arrays or Counters for collecting sufficient statistics
	  // from the training data.
	  public CounterMap<String,String> sourceTargetProbs=new CounterMap<String,String>();
	  public CounterMap<String,String> targetSourceProbs=new CounterMap<String,String>();
	  public Map<String,Map<String,Double>> unionProbs=new HashMap<>();
	  List<SentencePair> trainPairs;
	  
	  public IBMModel1(){
	  }
	  
	  public IBMModel1(CounterMap<String,String> sourceTargetTwice,List<SentencePair> pairs){
		  this.sourceTargetProbs = sourceTargetTwice;
		  this.trainPairs = pairs;
	  }

	  public void train(List<SentencePair> trainingPairs) {
		trainPairs = trainingPairs;
	    System.out.println("IBMModel1 beginning training....");

//	    CounterMap<String, String> t = new CounterMap<String,String>();

	    for(SentencePair pair : trainingPairs){
		      Set<String> sourceWords = new HashSet<String>(pair.getSourceWords());
		      Set<String> targetWords = new HashSet<String>(pair.getTargetWords());
		      for(String f:sourceWords){
		    	  sourceTargetProbs.setCountAsSource(f,sourceWords,targetWords, 1.0/(double)targetWords.size());
		      }
		    }
	    
	    CounterMap<String, String> c =new CounterMap<String,String>();
	    
//	    for(String s:sourceTargetProbs.keySet()){
//	    	Double total = getTotal(sourceTargetProbs.getCounter(s));
//			  for(Entry<String, Double> e:sourceTargetProbs.getCounter(s).entries.entrySet()){
//				  c.getCounter(s).setCount(e.getKey(), e.getValue()/total);
//			  }
//	    }
//	    sourceTargetProbs=c;
	    
	    for(String s:sourceTargetProbs.keySet()){
	    	Entry<String, Double> max = sourceTargetProbs.getCounter(s).maxEntry();
	    	if(max!=null){
	    		targetSourceProbs.incrementCount(max.getKey(), s, max.getValue());
	    	}
	    }
	    
	    for(SentencePair pair : trainingPairs){
		      Set<String> sourceWords = new HashSet<String>(pair.getSourceWords());
		      Set<String> targetWords = new HashSet<String>(pair.getTargetWords());
		      for(String f:targetWords){
		    	  targetSourceProbs.setCountAsSource(f,targetWords,sourceWords, 1.0/(double)sourceWords.size());
		      }
		    }
	    
	    c =new CounterMap<String,String>();
	    
	    for(String s:targetSourceProbs.keySet()){
	    	Double total = getTotal(targetSourceProbs.getCounter(s));
			  for(Entry<String, Double> e:targetSourceProbs.getCounter(s).entries.entrySet()){
				  c.getCounter(s).setCount(e.getKey(), e.getValue()/total);
			  }
	    }
	    targetSourceProbs = c;
	    
	    for(String t:targetSourceProbs.keySet()){
	    	Entry<String, Double> targetMax = targetSourceProbs.getCounter(t).maxEntry();
	    	Entry<String, Double> sourceMax = targetMax==null?null:sourceTargetProbs.getCounter(targetMax.getKey()).maxEntry();
	    	if(sourceMax!=null&&t.equals(sourceMax.getKey())){
	    		addElement(t, targetMax.getKey(),1.0);
	    	}
	    	else{
	    		addElement(t, targetMax.getKey(),1.0);
	    	}
	    }
	    
	    for(String s:sourceTargetProbs.keySet()){
	    	//chine->compagnie->company
	    	Entry<String, Double> sourceMax = sourceTargetProbs.getCounter(s).maxEntry();
			  Entry<String, Double> targetMax = sourceMax==null?null:targetSourceProbs.getCounter(sourceMax.getKey()).maxEntry();
			  if(sourceMax==null){
				  continue;
			  }
			  if(targetMax==null){
		    		addElement(sourceMax.getKey(), s,sourceMax.getValue());
			  }else if(targetMax!=null&&s.equals(targetMax.getKey())){
				  addElement(sourceMax.getKey(), s,1.0);
			  }else{
				  addElement(sourceMax.getKey(), s,0.01);
			  }
	    }


//	    sourceTargetProbs = t;
	  }
	  
	  private String listToString(Set<String> list){
		  String result = "";
		  for(String s:list){
			  result += s + " ";
		  }
		  return result.trim();
	  }
	  
	  private void addElement(String key,String value,Double prob){
		  Map<String,Double> m;
		  if(!unionProbs.containsKey(key)){
			  m = new HashMap<>();
			  m.put(value, prob);
			  unionProbs.put(key, m);
		  }else{
			  m=unionProbs.get(key);
			  m.put(value, prob);
			  unionProbs.put(key, m);
		  }
	    		
	  }
	  
	  private Double getTotal(Counter<String> counters){
		  Double totalValue = 0.0;
		  for(Entry<String, Double> e:counters.entries.entrySet()){
			  totalValue += e.getValue();
		  }
		  return totalValue;
	  }
	  
	  public void getSimilarity(String source,String target){
		  System.out.println(sourceTargetProbs.getCount(source, target));
	  }
	  
	  public String getMaxSimilar(String source){
//			  if(sourceTargetProbs.getCounter(source).maxIsUnique()){
				  return sourceTargetProbs.getCounter(source).argMax();
//			  }		  
//		  return null;
	  }
	  
	  public String getOptimumSimilarAsTarget(String source){
		  Map<String, Double> simMap =Utils.sortByValue(targetSourceProbs.getCounter(source).entries,false); 
		  if(simMap!=null && simMap.size()>0){
			  Iterator<Entry<String, Double>> i =simMap.entrySet().iterator();
			  String result =i.next().getKey();
			  return result;
		  }else return null;
//		  if(unionProbs.get(source)!=null){
//			  Map<String,Double> map = Utils.sortByValue(unionProbs.get(source), false);
//			  String result =map.entrySet().iterator().next().getKey();
//			  if(itDicTable.containsKey(result)){
//				  System.out.print(result + " >>>> ");
//				  getSimilarValuesAsTarget(result);
//			  }
//			  return result;
//		  }
//		  else return null;
	  }
	  
	  public String getCommonSimilar(String source){
		  String sourceMax = sourceTargetProbs.getCounter(source).argMax();
		  String targetMax = sourceMax==null?null:targetSourceProbs.getCounter(sourceMax).argMax();
		  if(targetMax!=null&&source.equals(targetMax)){
			  return sourceMax;
		  }else{
			  return null;
		  }
  }
	  
	  public String getMaxSimilarAsTarget(String source){
			  return targetSourceProbs.getCounter(source).argMax();
      }
	  
	  public double getMaxIndex(String source){
//		  Map<String, Double> simMap =Utils.sortByValue(sourceTargetProbs.getCounter(source).entries,false); 
//		  if(simMap.size()>1){
//			  double firstElement = Utils.getEntry(simMap, 0).getValue();
//			  double secondElement = Utils.getEntry(simMap, 1).getValue();
//			  if(((firstElement-secondElement)/(firstElement+secondElement))>=App.THRESHOLD_VALUE){
//				  return ((firstElement-secondElement)/(firstElement+secondElement));
//			  }else{
//				  return 0;
//			  }
//		  }else{
			  return sourceTargetProbs.getCounter(source).maxValue();
//		  }
	  }
	  
	  public double getMaxIndexAsTarget(String source){
//		  Map<String, Double> simMap =Utils.sortByValue(targetSourceProbs.getCounter(source).entries,false); 
//		  if(simMap.size()>1){
//			  double firstElement = Utils.getEntry(simMap, 0).getValue();
//			  double secondElement = Utils.getEntry(simMap, 1).getValue();
//			  if(((firstElement-secondElement)/(firstElement+secondElement))>=App.THRESHOLD_VALUE){
//				  return ((firstElement-secondElement)/(firstElement+secondElement));
//			  }else{
//				  return 0;
//			  }
//		  }else{
			  return targetSourceProbs.getCounter(source).maxValue();
//		  }
	  }
	  
	  public String getMaxSimilarWithTreshold(String source){
//		  Double totalValue = 0.0;
		  Map<String, Double> simMap =Utils.sortByValue(sourceTargetProbs.getCounter(source).entries,false); 
		  if(simMap.size()>1){
			  double firstElement = Utils.getEntry(simMap, 0).getValue();
			  double secondElement = Utils.getEntry(simMap, 1).getValue();
			  if(((firstElement-secondElement)/(firstElement+secondElement))>App1.THRESHOLD_VALUE){
				  return Utils.getEntry(simMap, 0).getKey();
			  }else{
				  return null;
			  }
		  }else{
			  return sourceTargetProbs.getCounter(source).argMax();
		  }
//		  for(Entry<String, Double> e:sourceTargetProbs.getCounter(source).entries.entrySet()){
//			  totalValue += e.getValue();
//		  }
//		  if(sourceTargetProbs.getCounter(source).maxValue()/totalValue>THRESHOLD_VALUE){
//			  return sourceTargetProbs.getCounter(source).argMax();
//		  }
//		  return null;
	  }
	  
	  public void getSimilarValues(String source){
		  System.out.println(sourceTargetProbs.getCounter(source).toString());
	  }
	  
	  public String getSimilarValuesAsTarget(String source){
		  return targetSourceProbs.getCounter(source).toString();
	  }
	  
	  public int getSimilarValueIndex(String source,String target){
		  if(sourceTargetProbs.getCounter(source)==null){
			  return -1;
		  }
		  int i=0;
		  for(Entry<String, Double> e:sourceTargetProbs.getCounter(source).entries.entrySet()){
			  ++i;
			  if(e.getKey().equals(target)){
				  return i;
			  }
		  }
		  return -1;
	  }
	  
	  public boolean isSimilarVaulesContain(String source,String target){
		  if(sourceTargetProbs.getCounter(source)==null){
			  return false;
		  }
		  for(Entry<String, Double> e:sourceTargetProbs.getCounter(source).entries.entrySet()){
			  if(e.getKey().equals(target)){
				  return true;
			  }
		  }
		  return false;
	  }
	  
	  public void writeResults(){
		  for(String s:sourceTargetProbs.keySet()){
			  System.out.println(s+ " : " + sourceTargetProbs.getCounter(s));
		  }
	  }

	@Override
	public Alignment align(SentencePair sentencePair) {
		// TODO Auto-generated method stub
		return null;
	}
	}
