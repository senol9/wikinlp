package hacettepe.edu.tr.wikiNLP.ibm;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class IBMModel implements WordAligner{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1406228897245799094L;
	public CounterMap<String,String> sourceTargetProbs=new CounterMap<String,String>();
    List<String> italianWords;
    List<String> englishWords;
    double[][] italianEnglishMatrix;

	@Override
	public Alignment align(SentencePair sentencePair) {
		return null;
	}

	@Override
	public void train(List<SentencePair> trainingData) {
		System.out.println("IBMModel beginning training....");
		CounterMap<String, String> cm = new CounterMap<String,String>();
		Set<String> itSet = new HashSet<>();
		Set<String> engSet = new HashSet<>();
	    for(SentencePair pair : trainingData){
		      for(String s:pair.getSourceWords()){
		    	  itSet.add(s);
		      }
		      for(String t:pair.getTargetWords()){
		    	  engSet.add(t);
		      }
		 }
	    
	    italianWords = new ArrayList<>(itSet);
	    itSet = null;
	    englishWords = new ArrayList<>(engSet);
	    engSet = null;
	    System.out.println("Words are wrapped.");
	    
	    Counter<String> total = new Counter<String>();
	    
	    for(SentencePair pair : trainingData){
		      List<String> sourceWords = pair.getSourceWords();
		      List<String> targetWords = pair.getTargetWords();
		      for(String f : sourceWords){
		    	double[] r = randomAssignment(targetWords.size());
		        for(int i = 0; i < targetWords.size(); i++){
		          cm.incrementCount(f, targetWords.get(i), r[i]);
		          total.incrementCount(f,  r[i]);
		        }
		      }
		 }
	    
	    System.out.println("Alignment propagation is completed.");
	    
	    for(SentencePair pair : trainingData){
	    	List<String> sourceWords = pair.getSourceWords();
		      List<String> targetWords = pair.getTargetWords();
		      for (String french_word : sourceWords){
			        for(String english_word : targetWords){
			            //debug(count.getCount(english_word, french_word));
			            double new_prob = cm.getCount(french_word , english_word) / total.getCount(french_word);
			            cm.setCount(french_word, english_word, new_prob);
			        }
			        }
	    }
	    
	    System.out.println("Normalization is completed.");
	    sourceTargetProbs = cm;
	}
	
	private double getSumOfRow(double[] row){
		double total = 0;
		for(double d:row){
			total +=d;
		}
		return total;
	}
	
    private double[] randomAssignment(int size) {
        double[] prob = new double[size];

        int total = 0;
        Random r = new Random();
        for (int i = 0; i < size; i++) {
            prob[i] = r.nextInt(size) + 1;
            total += prob[i];
        }
        return normalizationOfRandomness(prob, total);
    }
    
    private double[] normalizationOfRandomness(double[] pros,int total){
        for (int i = 0; i < pros.length; i++) {
            pros[i] = pros[i] / total;
        }
        return pros;
    }
	
	private void addToList(String word,List<String> list){
		if(!list.contains(word)){
			list.add(word);
		}
	}

	@Override
	public String getMaxSimilar(String source) {
		return sourceTargetProbs.getCounter(source).argMax();
	}
	
    private int getMaxValueIndex(double[] array) {
        int maxIndex = 0;
        double maxValue = array[0];

        for (int i = 0; i < array.length; i++) {
            if (array[i] > maxValue) {
                maxIndex = i;
                maxValue = array[i];
            }
        }
        return maxIndex;
    }

}
