package hacettepe.edu.tr.wikiNLP.ibm;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import hacettepe.edu.tr.wikiNLP.Constant;
import hacettepe.edu.tr.wikiNLP.FileController;

import java.util.Set;


public class CounterMap<K, V> implements Serializable,Cloneable{

	  /**
	 * 
	 */
	private static final long serialVersionUID = 7166888844141573877L;
	private MapFactory<V, Double> mf;
	  private Map<K, Counter<V>> counterMap;
//	  Hashtable<String, Integer> commonTable = FileController.readFileToHashTable(Constant.ENG_IT_COMMON_PATH);

	  // -----------------------------------------------------------------------

	  public CounterMap() {
	    this(new MapFactory.HashMapFactory<K, Counter<V>>(), 
	         new MapFactory.HashMapFactory<V, Double>());
	  }

	  public CounterMap(MapFactory<K, Counter<V>> outerMF, 
	                    MapFactory<V, Double> innerMF) {
	    mf = innerMF;
	    counterMap = outerMF.buildMap();
	  }

	  // -----------------------------------------------------------------------

	  protected Counter<V> ensureCounter(K key) {
	    Counter<V> valueCounter = counterMap.get(key);
	    if (valueCounter == null) {
	      valueCounter = new Counter<V>(mf);
	      counterMap.put(key, valueCounter);
	    }
	    return valueCounter;
	  }

	  /**
	   * Returns the keys that have been inserted into this CounterMap.
	   */
	  public Set<K> keySet() {
	    return counterMap.keySet();
	  }
	  
	  public void setCount(K key, V value, double count) {
		    Counter<V> valueCounter = ensureCounter(key);
		    valueCounter.setCount(value, count);
		  }

	  /**
	   * Sets the count for a particular (key, value) pair.
	   */
	  public void setCountAsSource(K key,Set<V> sourceList,Set<V> targetList, double count) {
		Counter<V> c = counterMap.get(key);
	    Counter<V> valueCounter = ensureCounter(key);
	    if(c!=null){
	    	//italian words
	    	Set<V> keySet = getKetSet(valueCounter);
	    	if(targetList.contains(key)){
//    			valueCounter.setIncCount((V) key, count);
//    			break;
    		}else if(containsList(keySet, targetList)){
    			  for(V v:keySet){
    				  if(targetList.contains(v)){
    					  valueCounter.setIncCount(v, 1.0/listContainCount(keySet, targetList));
    				  }
    			  }
    		}else{
    			for(V t:targetList){
    				valueCounter.setIncCount(t, count);
    			}
    		}
//	    	for(V k:keySet){
//	    		if(targetList.contains(key)){
////	    			valueCounter.setIncCount((V) key, count);
//	    			break;
//	    		}else if(targetList.contains(k)){
//	    			valueCounter.setIncCount(k, count);
//	    		}else{
//	    			valueCounter.setIncCount(k, -1);
//	    		}
//	    	}
//	    	for(V t:targetList){
////	    		if(valueCounter.containsKey(t)){
//	    			valueCounter.setIncCount(t, count);
////	    		}
//	    	}
	    }else{
	    	if(targetList.contains(key)){
	    		counterMap.remove(key);
	    	}else{
		    	for(V t:targetList){
		    	  valueCounter.setIncCount(t, count);
		    	}
	    	}

	    }
	    
	  }
	  
	  public void setCountAsTarget(K key,Set<V> sourceList,Set<V> targetList, double count) {
		Counter<V> c = counterMap.get(key);
	    Counter<V> valueCounter = ensureCounter(key);
	    if(c!=null){
	    	//italian words
	    	Set<V> keySet = getKetSet(valueCounter);
	    	if(targetList.contains(key)){
    			valueCounter.setIncCount((V) key, count);
//    			break;
    		}else if(containsList(keySet, targetList)){
    			  for(V v:keySet){
    				  if(targetList.contains(v)){
    					  valueCounter.setIncCount(v, 1);
    				  }
    			  }
    		}else{
    			for(V t:targetList){
    				valueCounter.setIncCount(t, count);
    			}
    		}
//	    	for(V k:keySet){
//	    		if(targetList.contains(key)){
////	    			valueCounter.setIncCount((V) key, count);
//	    			break;
//	    		}else if(targetList.contains(k)){
//	    			valueCounter.setIncCount(k, count);
//	    		}else{
//	    			valueCounter.setIncCount(k, -1);
//	    		}
//	    	}
//	    	for(V t:targetList){
////	    		if(valueCounter.containsKey(t)){
//	    			valueCounter.setIncCount(t, count);
////	    		}
//	    	}
	    }else{
//	    	if(targetList.contains(key)){
//	    		counterMap.remove(key);
//	    	}else{
		    	for(V t:targetList){
		    	  valueCounter.setIncCount(t, count);
		    	}
//	    	}

	    }
	    
	  }
	  
	  private boolean containsList(Set<V> keySet,Set<V> targetList){
		  for(V v:keySet){
			  if(targetList.contains(v)){
				  return true;
			  }
		  }
		  return false;
	  }
	  
	  private double listContainCount(Set<V> keySet,Set<V> targetList){
		  double i=0;
		  for(V v:keySet){
			  if(targetList.contains(v)){
				  ++i;
			  }
		  }
		  return i;
	  }
	  
	  private Set<V> getKetSet( Counter<V> valueCounter){
		  Set<V> resultSet = new HashSet<>();
		  for(Entry<V, Double> e:valueCounter.entries.entrySet()){
			  resultSet.add(e.getKey());
		  }
		  return resultSet;
	  }

	  /**
	   * Increments the count for a particular (key, value) pair.
	   */
	  public void incrementCount(K key, V value, double count) {
	    Counter<V> valueCounter = ensureCounter(key);
	    valueCounter.incrementCount(value, count);
	  }

	  /**
	   * Gets the count of the given (key, value) entry, or zero if that
	   * entry is not present.  Does not create any objects.
	   */
	  public double getCount(K key, V value) {
	    Counter<V> valueCounter = counterMap.get(key);
	    if (valueCounter == null)
	      return 0.0;
	    return valueCounter.getCount(value);
	  }

	  /**
	   * Gets the sub-counter for the given key.  If there is none, a
	   * counter is created for that key, and installed in the CounterMap.
	   * You can, for example, add to the returned empty counter directly
	   * (though you shouldn't).  This is so whether the key is present or
	   * not, modifying the returned counter has the same effect (but
	   * don't do it).
	   */
	  public Counter<V> getCounter(K key) {
	    return ensureCounter(key);
	  }

	  /**
	   * Returns the total of all counts in sub-counters.  This
	   * implementation is linear; it recalculates the total each time.
	   */
	  public double totalCount() {
	    double total = 0.0;
	    for (Map.Entry<K, Counter<V>> entry : counterMap.entrySet()) {
	      Counter<V> counter = entry.getValue();
	      total += counter.totalCount();
	    }
	    return total;
	  }

	  /**
	   * Returns the total number of (key, value) entries in the
	   * CounterMap (not their total counts).
	   */
	  public int totalSize() {
	    int total = 0;
	    for (Map.Entry<K, Counter<V>> entry : counterMap.entrySet()) {
	      Counter<V> counter = entry.getValue();
	      total += counter.size();
	    }
	    return total;
	  }

	  /**
	   * The number of keys in this CounterMap (not the number of
	   * key-value entries -- use totalSize() for that)
	   */
	  public int size() {
	    return counterMap.size();
	  }

	  /**
	   * True if there are no entries in the CounterMap (false does not
	   * mean totalCount > 0)
	   */
	  public boolean isEmpty() {
	    return size() == 0;
	  }

	  public String toString() {
	    StringBuilder sb = new StringBuilder("[\n");
	    for (Map.Entry<K, Counter<V>> entry : counterMap.entrySet()) {
	      sb.append("  ");
	      sb.append(entry.getKey());
	      sb.append(" -> ");
	      sb.append(entry.getValue());
	      sb.append("\n");
	    }
	    sb.append("]");
	    return sb.toString();
	  }
	  
	  public CounterMap<K, V> clone(){
	        try {
				return (CounterMap<K, V>) super.clone();
			} catch (CloneNotSupportedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        return null;
	    }

	  // -----------------------------------------------------------------------

	  public static void main(String[] args) {
	    CounterMap<String, String> bigramCounterMap = new CounterMap<String, String>();
	    bigramCounterMap.incrementCount("people", "run", 1);
	    bigramCounterMap.incrementCount("cats", "growl", 2);
	    bigramCounterMap.incrementCount("cats", "scamper", 3);
	    System.out.println(bigramCounterMap);
	    System.out.println("Entries for cats: " + bigramCounterMap.getCounter("cats"));
	    System.out.println("Entries for dogs: " + bigramCounterMap.getCounter("dogs"));
	    System.out.println("Count of cats scamper: " + bigramCounterMap.getCount("cats", "scamper"));
	    System.out.println("Count of snakes slither: " + bigramCounterMap.getCount("snakes", "slither"));
	    System.out.println("Total size: " + bigramCounterMap.totalSize());
	    System.out.println("Total count: " + bigramCounterMap.totalCount());
	    System.out.println(bigramCounterMap);
	  }
	}
