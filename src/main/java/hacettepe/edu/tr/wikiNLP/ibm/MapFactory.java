package hacettepe.edu.tr.wikiNLP.ibm;

import java.io.Serializable;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.WeakHashMap;

public abstract class MapFactory<K,V> implements Serializable {

	  public static class HashMapFactory<K,V> extends MapFactory<K,V> {
	    public Map<K,V> buildMap() {
	      return new HashMap<K,V>();
	    }
	  }

	  public static class IdentityHashMapFactory<K,V> extends MapFactory<K,V> {
	    public Map<K,V> buildMap() {
	      return new IdentityHashMap<K,V>();
	    }
	  }

	  public static class TreeMapFactory<K,V> extends MapFactory<K,V> {
	    public Map<K,V> buildMap() {
	      return new TreeMap<K,V>();
	    }
	  }

	  public static class WeakHashMapFactory<K,V> extends MapFactory<K,V> {
	    public Map<K,V> buildMap() {
	      return new WeakHashMap<K,V>();
	    }
	  }

	  public abstract Map<K,V> buildMap();
	}
