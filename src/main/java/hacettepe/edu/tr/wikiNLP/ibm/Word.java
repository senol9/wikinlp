package hacettepe.edu.tr.wikiNLP.ibm;

import java.io.Serializable;

public class Word implements Serializable,Comparable<Word>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4532394190427852538L;
	
	private String source;
	private String target;
	private String itWord;
	private String engWord;
	private String unionWord;
	private double itProb;
	private double engProb;
	
	
	public Word(String source, String target, String itWord, String engWord, String unionWord, double itProb,
			double engProb) {
		super();
		this.source = source;
		this.target = target;
		this.itWord = itWord;
		this.engWord = engWord;
		this.unionWord = unionWord;
		this.itProb = itProb;
		this.engProb = engProb;
	}
	public String getItWord() {
		return itWord;
	}
	public void setItWord(String itWord) {
		this.itWord = itWord;
	}
	public String getEngWord() {
		return engWord;
	}
	public void setEngWord(String engWord) {
		this.engWord = engWord;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	
	public double getItProb() {
		return itProb;
	}
	public void setItProb(double itProb) {
		this.itProb = itProb;
	}
	public double getEngProb() {
		return engProb;
	}
	public void setEngProb(double engProb) {
		this.engProb = engProb;
	}
	
	public String getUnionWord() {
		return unionWord;
	}
	public void setUnionWord(String unionWord) {
		this.unionWord = unionWord;
	}
	public String toString(){
		return source + " (" + itWord + ") >>> " + itProb + " - " + target + " (" + engWord + ")" + " >>> " + engProb + " - " + unionWord;
		
	}
	@Override
	public int compareTo(Word o) {
		// TODO Auto-generated method stub
		if(source==null){
			return this.itWord.compareTo(o.itWord);
		}
		return this.source.compareTo(o.source);
	}

}
