package hacettepe.edu.tr.wikiNLP.ibm;

import java.util.List;
import java.util.Set;

public class SentencePair {
	  int sentenceID;
	  List<String> targetWords;
	  List<String> sourceWords;

	  public int getSentenceID() {
	    return sentenceID;
	  }

	  public List<String> getTargetWords() {
	    return targetWords;
	  }

	  public List<String> getSourceWords() {
	    return sourceWords;
	  }
	  
	  public SentencePair(List<String> sourceWords, List<String> targetWords) {
		    this.targetWords = targetWords;
		    this.sourceWords = sourceWords;
		  }
	}
