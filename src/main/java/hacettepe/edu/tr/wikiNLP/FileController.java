package hacettepe.edu.tr.wikiNLP;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;

public class FileController {
	
	public static String decompressAsTitle(String zipFileName) {
		String unzipped = "";
		try {
			ZipFile zipFile = new ZipFile(zipFileName);
			Enumeration<? extends ZipEntry> entries = zipFile.entries();
			InputStream stream = null;
			while(entries.hasMoreElements()){
		        ZipEntry entry = entries.nextElement();
		        stream = zipFile.getInputStream(entry);
		    }
			Reader decoder = new InputStreamReader(stream);
			BufferedReader buffered = new BufferedReader(decoder);
			
			String line = buffered.readLine();
			int i =0;
			boolean isWritable = false;
			while (line != null&&i<100000) {
				if(line.contains("<article lang=\"en\"")){
					unzipped+=line+" --- ";
				}else if(line.contains("<article lang=\"tr\"")){
					unzipped+=line+"\n";
					++i;
					if(i%1000==0){
						isWritable=true;
					}
				}
				if(isWritable){
					writeFile(unzipped, "resources//"+(i/1000)+".txt");
					unzipped="";
					isWritable = false;
				}
				line = buffered.readLine();
//				System.out.println(line);
			}
			System.out.println(i);
			
			buffered.close();
			decoder.close();
			
			return unzipped;
		}
		catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String decompress(String zipFileName) {
		String unzipped = "";
		try {
			ZipFile zipFile = new ZipFile(zipFileName);
			Enumeration<? extends ZipEntry> entries = zipFile.entries();
			InputStream stream = null;
			while(entries.hasMoreElements()){
		        ZipEntry entry = entries.nextElement();
		        stream = zipFile.getInputStream(entry);
		    }
			Reader decoder = new InputStreamReader(stream);
			BufferedReader buffered = new BufferedReader(decoder);
			
			String line = buffered.readLine();
			int i =0;
			boolean isStart=false;
			while (line != null && !line.contains("<articlePair id=\"101\">")) {
				++i;
				if(line.contains("<articlePair id=\"1\">")){
					isStart=true;
				}
				if(isStart)
				unzipped+=line+"\n";
				line = buffered.readLine();
//				System.out.println(line);
			}
			
			buffered.close();
			decoder.close();
			
			return unzipped;
		}
		catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public static String countBz2(String zipFileName) {
		String unzipped = "";
		try {
			 FileInputStream fin = new FileInputStream(zipFileName);
			    BufferedInputStream bis = new BufferedInputStream(fin);
			    BZip2CompressorInputStream bzIn = new BZip2CompressorInputStream(fin);
			    BufferedReader buffered = new BufferedReader(new InputStreamReader(bzIn));
			
			String line = buffered.readLine();
			int i =0;
			while (line != null && i<10000) {
				
				if(line.contains("<articlePair id=\"")){
					++i;
				}
				line = buffered.readLine();
//				System.out.println(line);
			}
			
			buffered.close();
			bzIn.close();
			System.out.println(i);
			return unzipped;
		}
		catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	public static String count(String zipFileName) {
		String unzipped = "";
		try {
			ZipFile zipFile = new ZipFile(zipFileName);
			Enumeration<? extends ZipEntry> entries = zipFile.entries();
			InputStream stream = null;
			while(entries.hasMoreElements()){
		        ZipEntry entry = entries.nextElement();
		        stream = zipFile.getInputStream(entry);
		    }
			Reader decoder = new InputStreamReader(stream);
			BufferedReader buffered = new BufferedReader(decoder);
			
			String line = buffered.readLine();
			int i =0;
			while (line != null) {
				
				if(line.contains("<articlePair id=\"")){
					++i;
				}
				line = buffered.readLine();
//				System.out.println(line);
			}
			
			buffered.close();
			decoder.close();
			System.out.println(i);
			return unzipped;
		}
		catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static List<String>  readFileToList(String fileName){
		List<String> list = new ArrayList<>();
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		    	if(line.contains("/")){
		    		line = line.substring(0, line.indexOf("/"));
		    	}
		    	list.add(line);
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public static Hashtable<String, Integer>  readFileToHashTable(String fileName){
		Hashtable<String, Integer>  table = new Hashtable<>();
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
		    String line;
		    while ((line = br.readLine()) != null) {
		    	table.put(line.trim(), 0);
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return table;
	}
	
	public static Map<String,String> readToMapFile(String fileName){
		Map<String,String> map = new HashMap<>();
		try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
		    String line;
		    while ((line = br.readLine()) != null) {
				String[] splits = line.split("\\s");
				if(splits.length>1){
					map.put(splits[0].trim(), splits[1].trim());
				}
		    }
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}
	
	public static <K,V> void writeMapToFile(Map<K, V> map, String fileName ){
		map = new TreeMap<K, V>(map);
		try {
			FileWriter writer = new FileWriter(fileName);
			for (Entry<K, V> e : map.entrySet()) {
				writer.write(e.getKey() + " >>> " + e.getValue() +"\n");
			}
			writer.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public static <E> void writeListToFile(List<E> list, String fileName ){
		try {
			FileWriter writer = new FileWriter(fileName);
			for (E e:list) {
				writer.write(e.toString() +"\n");
			}
			writer.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	public static void writeFile(String content, String filename){
		try {
			File file = new File(filename);
			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(content);
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		List<String> engList = FileController.readFileToList("resources//eng.dic");
		List<String> itList = FileController.readFileToList("resources//it.dic");
		List<String> commonList = new ArrayList<>();
		for(String s:engList){
			if(itList.contains(s) && s.length()>3){
				commonList.add(s);
			}
		}
		FileController.writeListToFile(commonList, "resources//commonEN_IT.dic");
	}

}
